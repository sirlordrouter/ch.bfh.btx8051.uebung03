import java.io.Console;

/**
 * Berner Fachhochschule
 * Medizininformatik BSc
 * @author Johannes Gnaegi
 * @version 24.09.2012
 * 
 * Errechnet die Jahre die benoetigt werden
 * um einen gegebenen Betrag mittels Zinsen
 * zu verdoppeln. 
 */
public class BmiCalculator {

	/*
	 * Variablen unterhalb der Klasse heissen Instanzvariablen. 
	 */
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		int kg = 80; 				//Gewicht asl Zahl in kg
		int cm = 180;				//Groesse als Zahl in cm
		double bmi = 0; 			//Initialisieren der Variablen
		String gewicht = "80";		//Gewicht als Text
		String groesse = "180"; 	//Groesse als Text
		
		/*
		 * Einlesen des Gewichts in kg sowie der Körpergrösse in 
		 * cm. 
		 */
		Console objConsole = System.console();
		if (objConsole != null) {
			gewicht = objConsole.readLine("Geben Sie ihr Gewicht in kg ein: "); 			
			kg = Integer.parseInt(gewicht);
			
			groesse = objConsole.readLine("Geben Sie ihre Koerpergroesse in cm ein: "); 			
			cm = Integer.parseInt(groesse);
		}
		
		
		bmi = CalcBmi(kg, cm); //Berechnen de BMIs 
		
		/*
		 * Zusammenstellung der Info 
		 * über die EInstufung des BMIs 
		 */
		String info = "Die Person ist "; 
		if (bmi<16) {
			info += "stark untergewichtig. "; 
		}
		else if (16 < bmi & bmi < 17) {
			info += "mässig untergewichtig. ";  
		}else if (17 < bmi & bmi < 18.5) {
			info += "leicht untergewichtig. "; 
		} else if (18.5 < bmi & bmi < 24.99) {
			info += "normalgewichtig. ";  
		}  else {
			info += "übergewichtig. "; 
		}
		
		/*
		 * Ausgabe der Resultate
		 */
		System.out.println("Mit der Groesse von "+ groesse + "cm"); 
		System.out.println("und dem Gewicht von "+ gewicht + "kg");
		System.out.println("ergibt sich ein BMI von: " + bmi);
		System.out.println(info);
	}

	/**
	 * Berechnet den BMI anhand des Gewichts in kg und 
	 * der Körperrösse in cm. 
	 * @param kg ein <code>int</code> der das Gewicht beschreibt.
	 * @param cm ein <code>int</code> der die Körpergrösse in cm beschreibt. 
	 */
	private static double CalcBmi(int kg, int cm) {
		
		double bmi; 
		double m = cm / 100.0; //Umwandeln von cm in m. 
			
		bmi = kg / Math.pow(m, 2);  		// Formel zur Berechung des BMIs 
		return Math.rint(bmi*100.0)/100.0; 	//Runden auf zwei Stellen. 
	}

}
